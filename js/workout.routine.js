var Routine = function(options) {
  var defaults = {
    'type': 'workout',
    'cycles': 3,
    'prepare': 5,
    'work': 30,
    'rest': 10,
    'sounds': {
      'prepare': 'sounds/drums.mp3',
      'work': 'sounds/warcry.mp3',
      'rest': 'sounds/dingdingding.mp3'
    },
    'descriptions': [],
    'extra': [],
    'source': ''
  };
  var options = jQuery.extend(defaults, options || {});
  this.getOptions = function() {
    return options;
  };
};
