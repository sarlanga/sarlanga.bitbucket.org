var $timer__controls__routines = $('#timer__controls__routines');
var $timer__display__countdown__current__time =
    $('#timer__display__countdown--current__time');
var $timer__display__countdown__current__percentage =
    $('#timer__display__countdown--current__percentage');
var $timer__instructions__status__percentage =
    $('#timer__instructions__status__percentage');
var $timer__instructions__name = $('#timer__instructions__name');
var $timer__instructions__video = $('#timer__instructions__video');
var $timer__instructions__status__description =
    $('#timer__instructions__status__description');
var $audio__rest = $('#audio__rest');
$audio__rest.get(0).volume = 0.3;
var $audio__work = $('#audio__work');
$audio__work.get(0).volume = 0.3;
var $audio__prepare = $('#audio__prepare');
$audio__prepare.get(0).volume = 0.3;
var $timer__instructions__extra = $('#timer__instructions__extra');
var $timer__display__countdown__total__percentage =
    $('#timer__display__countdown--total__percentage');
var $timer__instructions__hr = $('#timer__instructions > hr');
$timer__controls__routines.change(function() {
  var locationHref = location.href;
  var $selection = $timer__controls__routines.find(':selected');
  var selectionType = $selection.data('type');

  if (typeof(selectionType) === 'undefined')
  {
    location.search = 'type=' + $selection.val();
  } else {
    location.search = 'routine=' + $selection.val() +
                      '&type=' + selectionType;
  }
});
for (var k in routines) {
  var kRountine = routines[k];
  if (typeof (kRountine) !== 'function') {
    if (typeof (kRountine['type']) === 'undefined') {
      kRountine['type'] = 'workout';
    }
    var $option = $('<li/>')
                  .append($('<a/>').attr('href',
                    '?routine=' + k + '&type=' + kRountine['type'])
                    .text(k));
    $timer__controls__routines.find('ul').append($option);
  }
}

var queryRoutine = location.search.match(/routine=(.+?)(?:&|$)/);
if (queryRoutine !== null) {
  $timer__controls__routines.val(queryRoutine[1]);
} else {
  var queryType = location.search.match(/type=(.+?)(?:&|$)/);
  if (queryType !== null) {
    queryType = queryType[1];
  } else {
    queryType = 'warmup';
  }
  var $typedRoutines =
      $timer__controls__routines.find('[href*="type=' + queryType + '"]');
  var newIdx = Math.floor(Math.random() * ($typedRoutines.length - 1));
  window.location.href = $typedRoutines.get(newIdx);
}
if (typeof(currentWork) !== 'undefined') {
  var $currentWork = $(currentWork);

  var resetCurrentCountdown = function(currentWork) {
    $timer__display__countdown__current__time
       .countdown((new Date()).getTime() + currentWork.getCurrentStep());
  };
  $timer__display__countdown__current__time
    .countdown(0).on('update.countdown', function(event) {
      $(this).text(event.strftime('%M:%S'));
    });
  var currentVideo = false;
  $currentWork.on('change', function() {
    $timer__display__countdown__current__percentage.animate({'width': '100%'},
        500, function() {
          $(this).animate(
              {'width': '0%'},
              currentWork.getCurrentStep() - 500
          );
        }
    );
    $timer__instructions__status__percentage.animate({'width': '0%'},
        500, function() {
          $(this).animate(
              {'width': '100%'},
              currentWork.getCurrentStep() - 500
          );
        }
    );
    var currentStep = currentWork.getCurrentWork();
    $timer__instructions__name.html(currentStep.info.name)
        .append($('<small/>').text('  next:' + currentWork.getNextWork().name));
    if (typeof(currentStep.info.video) !== 'undefined' &&
        currentStep.video !== '' && currentStep.info.video !== currentVideo) {
      currentVideo = currentStep.info.video;
      if ($timer__instructions__video.attr('src') === 'about:blank') {
        $timer__instructions__video.attr('src', currentStep.info.video);
      } else if (typeof(player.loadVideoByUrl) !== 'undefined') {
        var startTime = currentStep.info.video.match(/start=(.+?)(?:&|$)/);
        if (startTime !== null) {
          player.loadVideoByUrl(currentStep.info.video, startTime[1]);
        } else {
          player.loadVideoByUrl(currentStep.info.video);
        }
      }
    }
    var setRestingBars = function($bar) {
      $bar.removeClass('progress-bar-warning').parent()
          .removeClass('progress-striped active');
    };
    var unsetRestingBars = function($bar) {
      $bar.addClass('progress-bar-warning').parent()
          .addClass('progress-striped active');
    };
    if (currentWork.isResting()) {
      setRestingBars($timer__instructions__status__percentage);
      setRestingBars($timer__display__countdown__current__percentage);
      $timer__instructions__status__description.html('rest');
      if (!/muted/.test(location.search)) {
        $audio__rest.get(0).play().catch(e => setTimeout(() => $audio__rest.get(0).play(), 500) && console.error(e));
      }
    } else {
      unsetRestingBars($timer__instructions__status__percentage);
      unsetRestingBars($timer__display__countdown__current__percentage);
      $timer__instructions__status__description.html('lets do it!');
      if (!/muted/.test(location.search)) {
        $audio__work.get(0).play().catch(e => setTimeout(() => $audio__work.get(0).play(), 500) && console.error(e));
      }
    }
    resetCurrentCountdown(this);
  });
  $currentWork.on('prepare', function() {
    if (!/muted/.test(location.search)) {
      $audio__prepare.get(0).play().catch(e => setTimeout(() => $audio__prepare.get(0).play(), 500) && console.error(e));
    }
  });
  $currentWork.on('started', function() {
    if (!/muted/.test(location.search))
    {
      var sounds = currentWork.getSounds();
      $audio__prepare.html('<source src="' +
          sounds.prepare + '" type="audio/mpeg">');
      $audio__work.html('<source src="' +
          sounds.work + '" type="audio/mpeg">');
      $audio__rest.html('<source src="' +
          sounds.rest + '" type="audio/mpeg">');
    }
    if (typeof (sounds) !== 'undefined')
    {
      $timer__instructions__extra.append($('<h4/>').html('Extras:'));
      var $extra = $(currentWork.getExtra());
      $extra.each(function() {
        var liExtra = $('<li/>');
        if (typeof(this.video) !== 'undefined') {
          liExtra.append(
              $('<button/>').attr('data-video', this.video)
                  .html(this.name).click(function() {
                var dataVideo = $(this).data('video');
                var startTime = dataVideo.match(/start=(.+?)(?:&|$)/);
                if (startTime !== null) {
                  player.loadVideoByUrl(dataVideo, startTime[1]);
                } else {
                  player.loadVideoByUrl(dataVideo);
                }
              })
          );
        } else {
          liExtra.html(this.name);
        }
        if (typeof(this.sets) !== 'undefined') {
          liExtra.append(' x ' + this.sets);
        }
        $timer__instructions__extra.append(liExtra);
      });
    }
    var source = currentWork.getSource();
    $('#timer__instructions__source').attr('href', source).html(source);
    var totalTime = currentWork.getTotalTime();
    $('#timer__display__countdown--total__time')
        .countdown(((new Date()).getTime() + totalTime)).on('update.countdown',
        function(event) {
      var offsetSeconds = (event.offset.minutes * 60) + event.offset.seconds;
      currentWork.nextStep(offsetSeconds);
      $(this).text(event.strftime('%M:%S'));
      $('#timer__instructions__now').html((new Date()).toLocaleTimeString());
     }).on('finish.countdown', function(event) {
      var offsetSeconds = (event.offset.minutes * 60) + event.offset.seconds;
      currentWork.nextStep(offsetSeconds);
      if (!/muted/.test(location.search)) {
        $audio__rest.get(0).play();
      }
    });
    $timer__display__countdown__total__percentage
        .animate({'width': '100%'}, 1000, function() {
      $(this).animate({'width': '0%'}, totalTime);
    });
    $timer__instructions__hr.animate({'width': '0%'}, 1000, function() {
      $(this).animate({'width': '100%'}, totalTime);
    });

    setYTPlayer();
  });

  $currentWork.on('paused', function() {
    $timer__display__countdown__total__percentage.stop();
    $timer__instructions__hr.stop();
    $timer__display__countdown__current__percentage.stop();
    $timer__instructions__status__percentage.stop();
  });

  var setYTPlayer = function() {
    window.player = {};
    window.onYouTubeIframeAPIReady = function() {
        player = new YT.Player('timer__instructions__video', {
            events: {
                'onReady': onPlayerReady
            }
        });
    };

    window.onPlayerReady = function(event) {
      player.playVideo();
      // Mute?!
      //player.mute(); //instead of this use below
      event.target.mute();
      //player.setVolume(0);
    };
    var tag = document.createElement('script');
    tag.src = '//www.youtube.com/iframe_api';
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  };

  currentWork.setReady();
  currentWork.start();
}

var idleTime = 0;
$(document).ready(function() {
    //Increment the idle time counter 5 seconds.
    var idleInterval = setInterval(timerIncrement, 1000); // 5 Seconds

    //Zero the idle timer on mouse movement.
    $(this).mousemove(function(e) {
        idleTime = 0;
        $('.section:first').removeClass('hide');
    });
    $(this).keypress(function(e) {
        idleTime = 0;
        $('.section:first').removeClass('hide');
    });
});

function timerIncrement() {
    idleTime = idleTime + 1;
    if (idleTime > 10) { // 10 Seconds
        $('.section:first').addClass('hide');
    }
}
