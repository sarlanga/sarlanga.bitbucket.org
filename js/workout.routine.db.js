var routines = {
  'quick_warmup': {
    'type': 'warmup',
    'cycles': 1,
    'prepare': 15,
    'work': 30,
    'rest': 0,
    'sounds': {
      'prepare': 'sounds/dingdingding.mp3',
      'work': 'sounds/dingdingding.mp3',
      'rest': 'sounds/dingdingding.mp3'
    },
    'descriptions': [
      {
        'name': 'Boxer Shuffle',
        'video': 'https://www.youtube.com/embed/' +
                 'ERdZqyorGfk?start=26&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Overhead Reach + Stretch',
        'video': 'https://www.youtube.com/embed/' +
                 'ERdZqyorGfk?start=62&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'High Knee March',
        'video': 'https://www.youtube.com/embed/' +
                 'ERdZqyorGfk?start=94&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Torso Twists',
        'video': 'https://www.youtube.com/embed/' +
                 'ERdZqyorGfk?start=122&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Toe Touch Kicks',
        'video': 'https://www.youtube.com/embed/' +
                 'ERdZqyorGfk?start=152&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Full Torso Circles',
        'video': 'https://www.youtube.com/embed/' +
                 'ERdZqyorGfk?start=182&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Lat Step Toe Touches',
        'video': 'https://www.youtube.com/embed/' +
                 'ERdZqyorGfk?start=211&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Squats',
        'video': 'https://www.youtube.com/embed/' +
                 'ERdZqyorGfk?start=241&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Jumping Jacks',
        'video': 'https://www.youtube.com/embed/' +
                 'ERdZqyorGfk?start=271&autoplay=true&enablejsapi=1'

      },
      {
        'name': 'High Knees',
        'video': 'https://www.youtube.com/embed/' +
                 'ERdZqyorGfk?start=302&autoplay=true&enablejsapi=1'
      }
    ],
    'source': 'https://www.youtube.com/watch?v=ERdZqyorGfk'
  },
  'quick_cooldown': {
    'type': 'cooldown',
    'cycles': 1,
    'prepare': 15,
    'work': 15,
    'rest': 0,
    'sounds': {
      'prepare': 'sounds/dingdingding.mp3',
      'work': 'sounds/dingdingding.mp3',
      'rest': 'sounds/dingdingding.mp3'
    },
    'descriptions': [
      {
        'name': 'Shoulder Rolls',
        'video': 'https://www.youtube.com/embed/' +
                 'i9u-svNHgao?start=25&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Arm Pulls',
        'video': 'https://www.youtube.com/embed/' +
                 'i9u-svNHgao?start=38&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Toe Touch Stretch',
        'video': 'https://www.youtube.com/embed/' +
                 'i9u-svNHgao?start=54&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Quads Stretch',
        'video': 'https://www.youtube.com/embed/' +
                 'i9u-svNHgao?start=70&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Switch Sides',
        'video': 'https://www.youtube.com/embed/' +
                 'i9u-svNHgao?start=83&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Warrior Pose',
        'video': 'https://www.youtube.com/embed/' +
                 'i9u-svNHgao?start=100&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Switch Sides',
        'video': 'https://www.youtube.com/embed/' +
                 'i9u-svNHgao?start=113&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Downward Dog',
        'video': 'https://www.youtube.com/embed/' +
                 'i9u-svNHgao?start=130&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Child\'s Pose',
        'video': 'https://www.youtube.com/embed/' +
                 'i9u-svNHgao?start=144&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Kneeling Calf Stretch',
        'video': 'https://www.youtube.com/embed/' +
                 'i9u-svNHgao?start=160&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Switch Sides',
        'video': 'https://www.youtube.com/embed/' +
                 'i9u-svNHgao?start=174&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Cobra Stretch',
        'video': 'https://www.youtube.com/embed/' +
                 'i9u-svNHgao?start=192&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Glute Stretch',
        'video': 'https://www.youtube.com/embed/' +
                 'i9u-svNHgao?start=212&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Switch Sides',
        'video': 'https://www.youtube.com/embed/' +
                 'i9u-svNHgao?start=227&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Torso Stretch',
        'video': 'https://www.youtube.com/embed/' +
                 'i9u-svNHgao?start=248&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Switch Sides',
        'video': 'https://www.youtube.com/embed/' +
                 'i9u-svNHgao?start=270&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Full Body Stretch',
        'video': 'https://www.youtube.com/embed/' +
                 'i9u-svNHgao?start=290&autoplay=true&enablejsapi=1'
      }
    ],
    'source': 'https://www.youtube.com/watch?v=i9u-svNHgao'
  },
  'no_equipment_conditioning': {
    'descriptions': [
      {
        'name': 'Burpees',
        'video': 'https://www.youtube.com/embed/' +
                 'PeOgB-xExVU?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Walking lunges',
        'video': 'https://www.youtube.com/embed/' +
                 'syQ-onZ9pjw?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'High knees',
        'video': 'https://www.youtube.com/embed/' +
                 'FhqmGltkOuc?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Pike push ups',
        'video': 'https://www.youtube.com/embed/' +
                 'YvHg9qHSdf4?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Burpee tuck jumps',
        'video': 'https://www.youtube.com/embed/' +
                 '1Y3aOUZuR8w?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'V ups',
        'video': 'https://www.youtube.com/embed/' +
                 '9ecEW2CQy5s?autoplay=true&enablejsapi=1'
      }
    ],
    'extra': [
      {
        'name': 'L-sit',
        'video': 'https://www.youtube.com/embed/' +
                 '2XXUfvZ2-oI?autoplay=true&enablejsapi=1'
      }
    ],
    'source': 'http://www.12minuteathlete.com/' +
            'no-equipment-conditioning-hiit-workout/'
  },
  'badass_bodyweight': {
    'descriptions': [
      {
        'name': 'Tuck jumps',
        'video': 'https://www.youtube.com/embed/' +
                 '4PX2x4NGyG0?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Pike push ups',
        'video': 'https://www.youtube.com/embed/' +
                 'YvHg9qHSdf4?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Burpee tuck jumps',
        'video': 'https://www.youtube.com/embed/' +
                 '1Y3aOUZuR8w?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Triceps push ups',
        'video': 'https://www.youtube.com/embed/' +
                 'ijzldjxvAA8?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Jump lunges',
        'video': 'https://www.youtube.com/embed/' +
                 '3HEBHtvEqXw?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'V ups',
        'video': 'https://www.youtube.com/embed/' +
                 '9ecEW2CQy5s?autoplay=true&enablejsapi=1'
      }
    ],
    'extra': [
      {
        'name': 'Mountain climbers', 'sets': 100,
        'video': 'https://www.youtube.com/embed/' +
                 'ufQFkVm-6qg?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Bridges', 'sets': 6,
        'video': 'https://www.youtube.com/embed/' +
                 '_3Em81SKpLU?autoplay=true&enablejsapi=1'
      }
    ],
    'source': 'http://www.12minuteathlete.com/badass-bodyweight-hiit-workout/'
  },
  'glutes_routine': {
    'descriptions': [
      {
        'name': 'Air squats',
        'video': 'https://www.youtube.com/embed/' +
                 '9mwUxMSTN_w?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Hip abduction 1',
        'video': 'https://www.youtube.com/embed/' +
                 'cz1gh_aYn5k?start=53&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Hip abduction 2',
        'video': 'https://www.youtube.com/embed/' +
                 'cz1gh_aYn5k?start=53&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Squat jumps',
        'video': 'https://www.youtube.com/embed/' +
                 're7G9FwFX2I?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Bulgarian Split Squat',
        'video': 'https://www.youtube.com/embed/' +
                 '2C-uNgKwPLE?start=56&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Rotating plank',
        'video': 'https://www.youtube.com/embed/' +
                 'UIDaXSXjuGc?start=102&autoplay=true&enablejsapi=1'
      }
    ]
  },
  'on_the_go_1': {
    'descriptions': [
      {
        'name': 'High knees',
        'video': 'https://www.youtube.com/embed/' +
                 'FhqmGltkOuc?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Reptile Push Ups',
        'video': 'https://www.youtube.com/embed/' +
                 '6k8-4BqutnE?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Tuck jumps',
        'video': 'https://www.youtube.com/embed/' +
                 '4PX2x4NGyG0?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Walking lunges',
        'video': 'https://www.youtube.com/embed/' +
                 'syQ-onZ9pjw?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'High knees',
        'video': 'https://www.youtube.com/embed/' +
                 'FhqmGltkOuc?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Sit Ups',
        'video': 'https://www.youtube.com/embed/' +
                 'ysHS5f1Ypc4?autoplay=true&enablejsapi=1'
      }
    ],
    'source': 'http://www.12minuteathlete.com/wp-content/hiit-on-the-go.pdf'
  },
  'on_the_go_2': {
    'descriptions': [
      {
        'name': 'Burpees',
        'video': 'https://www.youtube.com/embed/' +
                 'PeOgB-xExVU?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Air squats',
        'video': 'https://www.youtube.com/embed/' +
                 '9mwUxMSTN_w?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'High knees',
        'video': 'https://www.youtube.com/embed/' +
                 'FhqmGltkOuc?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Mountain climbers',
        'video': 'https://www.youtube.com/embed/' +
                 'ufQFkVm-6qg?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Squat jumps',
        'video': 'https://www.youtube.com/embed/' +
                 're7G9FwFX2I?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Pistols',
        'video': 'https://www.youtube.com/embed/' +
                 'c-VCXKkLAFY?autoplay=true&enablejsapi=1'
      }
    ],
    'source': 'http://www.12minuteathlete.com/wp-content/hiit-on-the-go.pdf'
  },
  'on_the_go_3': {
    'descriptions': [
      {
        'name': 'Tuck jumps',
        'video': 'https://www.youtube.com/embed/' +
                 '4PX2x4NGyG0?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Handstand push ups',
        'video': 'https://www.youtube.com/embed/' +
                 'OdY5XJ21tRY?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Burpees',
        'video': 'https://www.youtube.com/embed/' +
                 'PeOgB-xExVU?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Pike jumps',
        'video': 'https://www.youtube.com/embed/' +
                 'tKyviI7NZkw?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Jump lunges',
        'video': 'https://www.youtube.com/embed/' +
                 '3HEBHtvEqXw?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'V ups',
        'video': 'https://www.youtube.com/embed/' +
                 '9ecEW2CQy5s?autoplay=true&enablejsapi=1'
      }
    ],
    'source': 'http://www.12minuteathlete.com/wp-content/hiit-on-the-go.pdf'
  },
  'on_the_go_4': {
    'descriptions': [
      {
        'name': 'High knees',
        'video': 'https://www.youtube.com/embed/' +
                 'FhqmGltkOuc?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Walking lunges',
        'video': 'https://www.youtube.com/embed/' +
                 'syQ-onZ9pjw?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Burpees',
        'video': 'https://www.youtube.com/embed/' +
                 'PeOgB-xExVU?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Dive bomber push ups',
        'video': 'https://www.youtube.com/embed/' +
                 'UDF-j9uiRvg?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'High knees',
        'video': 'https://www.youtube.com/embed/' +
                 'FhqmGltkOuc?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Mountain climbers',
        'video': 'https://www.youtube.com/embed/' +
                 'ufQFkVm-6qg?autoplay=true&enablejsapi=1'
      }
    ],
    'source': 'http://www.12minuteathlete.com/wp-content/hiit-on-the-go.pdf'
  },
  'on_the_go_5': {
    'descriptions': [
      {
        'name': 'Burpees',
        'video': 'https://www.youtube.com/embed/' +
                 'PeOgB-xExVU?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Pistols',
        'video': 'https://www.youtube.com/embed/' +
                 'c-VCXKkLAFY?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Squat jumps',
        'video': 'https://www.youtube.com/embed/' +
                 're7G9FwFX2I?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Handstand push ups',
        'video': 'https://www.youtube.com/embed/' +
                 'OdY5XJ21tRY?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Tuck jumps',
        'video': 'https://www.youtube.com/embed/' +
                 '4PX2x4NGyG0?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'V ups',
        'video': 'https://www.youtube.com/embed/' +
                 '9ecEW2CQy5s?autoplay=true&enablejsapi=1'
      },
    ],
    'source': 'http://www.12minuteathlete.com/wp-content/hiit-on-the-go.pdf'
  },
  'on_the_go_6': {
    'cycles': 4,
    'descriptions': [
      {
        'name': 'Burpees',
        'video': 'https://www.youtube.com/embed/' +
                 'PeOgB-xExVU?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Air squats',
        'video': 'https://www.youtube.com/embed/' +
                 '9mwUxMSTN_w?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Jump lunges',
        'video': 'https://www.youtube.com/embed/' +
                 '3HEBHtvEqXw?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Push ups',
        'video': 'https://www.youtube.com/embed/' +
                 '4Xn6VEB2nZA?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'High knees',
        'video': 'https://www.youtube.com/embed/' +
                 'FhqmGltkOuc?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Sit Ups',
        'video': 'https://www.youtube.com/embed/' +
                 'ysHS5f1Ypc4?autoplay=true&enablejsapi=1'
      }
    ],
    'source': 'http://www.12minuteathlete.com/wp-content/hiit-on-the-go.pdf'
  },
  'on_the_go_7': {
    'cycles': 4,
    'descriptions': [
      {
        'name': 'Tuck jumps',
        'video': 'https://www.youtube.com/embed/' +
                 '4PX2x4NGyG0?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Reptile Push Ups',
        'video': 'https://www.youtube.com/embed/' +
                 '6k8-4BqutnE?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'High knees',
        'video': 'https://www.youtube.com/embed/' +
                 'FhqmGltkOuc?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Walking lunges',
        'video': 'https://www.youtube.com/embed/' +
                 'syQ-onZ9pjw?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Burpees',
        'video': 'https://www.youtube.com/embed/' +
                 'PeOgB-xExVU?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'V ups',
        'video': 'https://www.youtube.com/embed/' +
                 '9ecEW2CQy5s?autoplay=true&enablejsapi=1'
      }
    ],
    'source': 'http://www.12minuteathlete.com/wp-content/hiit-on-the-go.pdf'
  },
  'better_sex': {
    'work': 30,
    'descriptions': [
      {
        'name': 'Cross leg push ups',
        'video': 'https://www.youtube.com/embed/' +
                 '5OtpYREunos?start=60&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Surfers',
        'video': 'https://www.youtube.com/embed/' +
                 '5OtpYREunos?start=100&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Side V-Crunch',
        'video': 'https://www.youtube.com/embed/' +
                 '5OtpYREunos?start=140&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Jump forward jump back',
        'video': 'https://www.youtube.com/embed/' +
                 '5OtpYREunos?start=180&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Push-up + knee tuck',
        'video': 'https://www.youtube.com/embed/' +
                 '5OtpYREunos?start=220&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'One leg bridge',
        'video': 'https://www.youtube.com/embed/' +
                 '5OtpYREunos?start=260&autoplay=true&enablejsapi=1'
      },
      {
        'name': 'One leg bridge (switch side)',
        'video': 'https://www.youtube.com/embed/' +
                 '5OtpYREunos?start=300&autoplay=true&enablejsapi=1'
      }
    ],
    'source': 'http://www.12minuteathlete.com/wp-content/hiit-on-the-go.pdf'
  },
  'test': {
    'type': 'test',
    'cycles': 2,
    'prepare': 3,
    'work': 10,
    'rest': 5,
    'sounds': {
      'prepare': 'sounds/dingdingding.mp3',
      'work': 'sounds/dingdingding.mp3',
      'rest': 'sounds/dingdingding.mp3'
    },
    'descriptions': [
      {
        'name': 'Shoulder Rolls',
        'video': 'https://www.youtube.com/embed/' +
                 'i9u-svNHgao?start=25&autoplay=true&enablejsapi=1'
      },
      {'name': 'Arm Pulls',
        'video': 'https://www.youtube.com/embed/' +
                 'i9u-svNHgao?start=38&autoplay=true&enablejsapi=1'
      },
      {'name': 'Arm Pulls',
        'video': 'https://www.youtube.com/embed/' +
                 'i9u-svNHgao?start=38&autoplay=true&enablejsapi=1'
      }
    ],
    'extra': [
      {
        'name': 'Mountain climbers', 'sets': 100,
        'video': 'https://www.youtube.com/embed/' +
                 'ufQFkVm-6qg?autoplay=true&enablejsapi=1'
      },
      {
        'name': 'Bridges',
        'sets': 6,
        'video': 'https://www.youtube.com/embed/' +
                 '_3Em81SKpLU?autoplay=true&enablejsapi=1'
      }
    ],
    'source': 'https://www.youtube.com/watch?v=i9u-svNHgao'
  }
};
