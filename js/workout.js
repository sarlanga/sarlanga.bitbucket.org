var Workout = function(routine) {
  var asyncTrigger = function(obj, event, params) {
    var _asyncTrigger = function(args) {
      console.log(args.event);
      $(args.obj).trigger(args.event, args.params);
    };
    setTimeout(_asyncTrigger, 1, {obj: obj, event: event, params: params});
  };
  var _currentRoutine = routine.getOptions();
  var _stepLenght = _currentRoutine.prepare * 1000;
  var _currentDbIndex = -1;
  var _currentDbCycle = 1;
  var _totalTime = -1;

  this.getTotalTime = function()
  {
    if (_totalTime === -1) {
      _totalTime = _currentRoutine.prepare * 1000 +
          ((_currentRoutine.rest + _currentRoutine.work) * 1000 *
          _currentRoutine.descriptions.length * _currentRoutine.cycles);
    }
    return _totalTime;
  };

  var _nextStep = function() {
    _isResting = _currentRoutine.rest === 0;
    _currentDbIndex += 1;
    if (_currentDbIndex !== -1) {
      _stepLenght = (_currentRoutine.rest + _currentRoutine.work) * 1000;
    }
    if (_currentDbIndex >= _currentRoutine.descriptions.length) {
      _currentDbIndex = 0;
      _currentDbCycle += 1;
    }
  };

  var _lastTimestamp = false;
  var _currentTick = 0;
  var _isResting = true;
  this.updateTick = function() {
    var time = new Date().getTime();
    if (_lastTimestamp) {
      _currentTick += (time - _lastTimestamp);
      console.log(_currentTick / 1000);
      _lastTimestamp = time;
      if (_currentTick >= _stepLenght) {
        _currentTick = _currentTick - _stepLenght;
        _nextStep();
      }
      if (_currentDbCycle > _currentRoutine.cycles) {
        _lastTimestamp = time = false;
        asyncTrigger(this, 'complete');
      } else if (_currentDbIndex !== -1) {
        if (!_isResting && _currentTick <= _currentRoutine.rest * 1000 &&
            _currentRoutine.rest !== 0) {
          _isResting = true;
          asyncTrigger(this, 'change');
        } else if (_isResting && _currentTick >= _currentRoutine.rest * 1000 &&
            _currentRoutine.work !== 0) {
          _isResting = false;
          asyncTrigger(this, 'change');
        }
      }
    }
    return time;
  };
  $(this).on('ready', function() {
    var counter = function(workout) {
      if (_lastTimestamp) {
        _lastTimestamp = workout.updateTick();
      }
      setTimeout(counter, 300, workout);
    };
    setTimeout(counter, 300, this);
  });

  this.start = function() {
    _lastTimestamp = new Date().getTime();
    asyncTrigger(this, 'started');
    asyncTrigger(this, 'change');
  };

  this.getCurrentWork = function()
  {
    var workType = 'prepare';
    var workIten = {
      'type': workType,
      'info': {
        'name': 'PREPARE'
      }
    };
    if (_currentDbIndex !== -1) {
      if (_isResting) {
        workType = 'rest';
      } else {
        workType = 'work';
      }
      workIten = {
        'type': workType,
        'info': _currentRoutine.descriptions[_currentDbIndex]
      };
    }
    return workIten;
  };
  this.getNextWork = function()
  {
    var nextIndex = _currentDbIndex + 1;
    if (nextIndex >= _currentRoutine.descriptions.length) {
      nextIndex = 0;
    }
    return _currentRoutine.descriptions[nextIndex];
  };
  this.getCurrentPercentage = function()
  {
    var currentDuration = _currentRoutine.rest;
    var currentlyElapsed = _currentTick / 1000;
    if (currentlyElapsed > currentDuration) {
      currentDuration = _currentRoutine.work;
      currentlyElapsed -= _currentRoutine.rest;
    }
    return currentlyElapsed / currentDuration;
  };

  this.getTotalPercentage = function()
  {
    var elapsed = _currentTick;
    if (_currentDbIndex !== -1) {
      elapsed += _currentRoutine.prepare * 1000;
      elapsed += (_currentDbCycle - 1) * _currentRoutine.descriptions.length *
          _stepLenght;
      elapsed += (_currentDbIndex) * (_stepLenght);
    }
    return elapsed / this.getTotalTime();
  };

  this.getCurrentStep = function() {
    var step = _currentRoutine.prepare;
    if (_currentDbIndex !== -1) {
      if (_isResting) {
        step = _currentRoutine.rest;
      } else {
        step = _currentRoutine.work;
      }
    }
    return step * 1000;
  };

  this.isResting = function()
  {
    return _isResting;
  };
  this.getExtra = function()
  {
    return _currentRoutine.extra;
  };
  this.getSounds = function()
  {
    return _currentRoutine.sounds;
  };
  this.getSource = function()
  {
    return _currentRoutine.source;
  };

  $(this).on('change', function(ev, timedIndex) {
    if (_currentDbIndex === -1) {
      asyncTrigger(this, 'prepare');
    } else if (_isResting) {
      asyncTrigger(this, 'restingChanged');
    } else if (!_isResting) {
      asyncTrigger(this, 'workChanged');
    }
  });
  this.nextStep = function() {};
  this.setReady = function() {};
  asyncTrigger(this, 'ready');
};
